#include "StateMachine.h"

//

State * Default;

//
//	CLASS StateMachine
//


//Initialize with starting State
void StateMachine::Initialize(State * FirstState)
{
	Start=FirstState;
	Default=Start;
	ActualState=Start;
	NextState=Default;
	this->Update();
}

void StateMachine::Runner()
{
	NextState=ActualState->Runner();
	this->Update();
}

void StateMachine::Update()
{
	//If not NULL
	if(NextState)
	{
		//If not NULL
		if(NextState->StateFunction)
		{
			ActualState=NextState;
		}
		//if not, stays here
	}
	else
	{
		ActualState=Default;
		NextState=Default;
	}
}

//
//	CLASS State
//

void State::Create()
{
	//Pointers to null
	StateFunction=0;
	DoEnter=0;
	DoOut=0;
	Next[0]=0;
	Next[1]=0;
	Active=0;
}

void State::Create(State * NextState,PointerToFunction Function,PointerToFunction DoWhenEnter,PointerToFunction DoWhenOut)
{
	//Set array of pointers to next state
	Next[0]=NextState;
	Next[1]=Next[0];
	Active=0;
	
	//Assing to state what it should do
	StateFunction=Function;
	DoEnter=DoWhenEnter;
	DoOut=DoWhenOut;
	
}

void State::Create(State * NextState1,State * NextState2,PointerToFunction Function,PointerToFunction DoWhenEnter,PointerToFunction DoWhenOut)
{
	//Set array of pointers to next state
	Next[0]=NextState1;
	Next[1]=NextState2;
	Active=0;
	
	//Assing to state what it should do
	StateFunction=Function;
	DoEnter=DoWhenEnter;
	DoOut=DoWhenOut;	
}

void State::Destroy()
{
	Next[0]=0;
	Next[1]=0;
	StateFunction=0;
	DoEnter=0;
	DoOut=0;
	Active=0;
	
	//Destroy all next states
	if(Next[0]->StateFunction)
	{
		Next[0]->Destroy();
	}
	if(Next[1]->StateFunction)
	{
		Next[1]->Destroy();
	}
}

State * State::Runner()
{
	byte NextID;
	NextID=255;
	
	if(!Active)
	{
		if(DoEnter)
			DoEnter();
	}	
	
	if(StateFunction) //if not null
		NextID=StateFunction();
		
	switch(NextID)
	{
		case 1:
			if(DoOut); //if not null
				DoOut();
			Active=0;
			return Next[0];
		break;
		case 2:
			if(DoOut); //if not null
				DoOut();
			Active=0;
			return Next[1];
		break;
		default: //no next state
			Active=1;
			return this; //stays here
	}
}
