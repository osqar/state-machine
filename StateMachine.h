#ifndef StateMachine_h
#define StateMachine_h

/* #include <Arduino.h>
#include <inttypes.h> */

//20/05/13 Added new pointers to functions: DoEnter and DoOut

#define True 1
#define False 0

typedef unsigned char uint8_t;
typedef uint8_t byte;
typedef uint8_t (*PointerToFunction)();

class State
{
	public:	
		//Constructor & Destructor
		void Create();
		void Create(State *,PointerToFunction,PointerToFunction,PointerToFunction);
		void Create(State *,State *,PointerToFunction,PointerToFunction,PointerToFunction);		
		void Destroy();
		
		State * Runner();
		
		//whatever state does 
		//returns Next state ID (1 if Next[0] and 2 if Next[1])
		PointerToFunction StateFunction;
		PointerToFunction DoEnter;
		PointerToFunction DoOut;
		//State Transitions: Array of pointers
		State * Next[2];
		bool Active;
};

class StateMachine
{
	public:
		void Initialize(State *);
	
		void Runner();
		void Update();
			
		State * Start;
		State * ActualState;
		State * NextState;
};

#endif

