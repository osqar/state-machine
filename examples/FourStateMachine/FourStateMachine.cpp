#include "StateMachine.h"
#include <stdio.h>
//if any state doesn't return anything else than 0, StateMachine will act like a loop

State FirstState,SecondState,ThirdState,FourthState;
StateMachine STM;
int i=0;

uint8_t FirstStateFunction()
{
	int Select=0;
    printf("\nThis is State 1");
    
    fflush(stdin);
	scanf("%d",&Select);
	fflush(stdin);
	printf("\n%d",Select);
	
	return Select;
}

uint8_t SecondStateFunction()
{
	int Select=0;
	
    printf("\nThis is State 2");
	
    fflush(stdin);
	scanf("%d",&Select);
	fflush(stdin);
	printf("\n%d",Select);	
	
	return Select;
}

uint8_t ThirdStateFunction()
{
	printf("\nThis is State 3");
	return 1;
}

uint8_t FourthStateFunction()
{
     printf("\nThis is last state");
     getchar();
     i=0;
     
     return 1;
}

void setup();
void loop();

int main()
{
 setup();
 do
 {
  loop();
 }while(True);
	return 1;
}

void setup()
{
    FirstState.Create(&SecondState,&FourthState,FirstStateFunction);
	SecondState.Create(&ThirdState,&FirstState,SecondStateFunction);
	ThirdState.Create(&FourthState,ThirdStateFunction);
	FourthState.Create(&FirstState,FourthStateFunction);
	
	STM.Initialize(&FirstState);
}

void loop()
{
	STM.Runner();
}
